public class SayHello {
    public static void main (String[] args) {
        if (args.length == 0) {
            System.out.println ("You must type in something");
            } else {
            try {
            int value = Integer.parseInt (args[0]);
            System.out.println ("Double of " + value + " is " + 2*value);
            } catch (NumberFormatException nfe) {
            System.out.println ("You must provide a number");
            }
    }

}
}